const methods = {
    "GET": "GET",
    'POST': "POST",
    "PUT": "PUT",
    "PATCH": "PATCH",
    "DELETE": "DELETE"
}

const header = {
    "PRIVATE-TOKEN" : "PRIVATE-TOKEN",
    "ACCEPT" : "accept",
    "CONTENT_TYPE" : "Content-Type"
}

const params = {
    "MEMBERSHIP" : "membership",
    "USER_ID" : "user_id",
    "ACCESS_LEVEL" : "access_level",
    "ID" : "id",
    "KEY" : "key",
    "VALUE" : "value",
    "NAME" : "name",
    "USERNAME" : "username",
    "Module" : "module",
    "DESCRIPTION" : "description",
    "INITIALIZE_WITH_README" : "initialize_with_readme",
}

const messages = {
    "INVALID_ENDPOINT" : "No such endpoint available",
    "UNSUPPORTED_OR_INVALID_METHOD" : "Is invalid or not supported at current time"
}

const status_codes = {
    "200" : "200",
    "401" : "401",
    "201" : "201"
}

module.exports = {methods, header, params, messages, status_codes};
