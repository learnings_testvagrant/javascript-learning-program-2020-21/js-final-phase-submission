const baseUrl = 'https://gitlab.com/api/v4';
const client = require('supertest')(baseUrl);
const {header} = require('../constants/constants');

let commonHeaders = new Map();
commonHeaders.set(header["PRIVATE-TOKEN"], process.env.PRIVATE_TOKEN);

let defaultHeaders = new Map();

defaultHeaders.set(header.ACCEPT, '*/*');
defaultHeaders.set(header.CONTENT_TYPE, 'application/json');

module.exports = {client, baseUrl, commonHeaders, defaultHeaders};