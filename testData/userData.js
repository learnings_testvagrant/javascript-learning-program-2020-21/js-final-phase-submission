const timeout = 8000;

let userInfo = [{
    'username' : 'manoy',
    'membership' : 'true'
}];

const endpoints = {
    'getUserWithoutAuth' : '/users',
    'getDetailedUserWithAuth' : '/user',
    'getProjectsList' : '/projects'
}

module.exports = {userInfo, endpoints, timeout};