const {generateRandom} = require('../utils/helper');

let userInfo = [{
    'username' : 'manoy',
    'membership' : 'true'
}];

let newProjectData = [
    {
        'name' : `${generateRandom('Test')}`,
        'description' : 'This project is created for learning purpose',
        'initialize_with_readme' : true
    }
];

let memberData = [
    {
        'user_id' : '6596681',
        'access_level' : 30
    }
];

let variables = [
    {
        'key' : `${generateRandom('Key')}`,
        'value' : `${generateRandom('Value')}`
    }
];

const projectEndpoints = {
    'createNewPrivateProject' : '/projects',
    'getMembersOfAProject' : '/projects/:id/members',
    'getProjectDetailsById' : '/projects/:id',
    'addMemberToAProject' : '/projects/:id/members',
    'getVariablesOfAProject' : '/projects/:id/variables',
    'addVariableToAProject' : '/projects/:id/variables',
    'deleteVariableFromAProject' : '/projects/:id/variables/:key',
    'deleteProject' : '/projects/:id',
}

module.exports = {userInfo, projectEndpoints, newProjectData, memberData, variables};