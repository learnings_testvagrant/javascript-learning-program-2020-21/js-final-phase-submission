const {endpoints, userInfo, timeout} = require("../testData/userData");
const expect = require('chai').expect;
const {getRandomUser, getByIndex} = require("../utils/helper");
const {params, methods} = require("../constants/constants");
const {projectEndpoints} = require("../testData/projectData");
const projectsService = require('../services/projects/projectRequests');
const responseService = require('../services/responses/responseService');
const userRequestService = require('../services/user/userRequests');

let response, preparedRequest;
let user, extractedInfo;

describe('APIs : Regression flow for existing gitlab user', () => {
    before('setUp', function () {
         user = getByIndex(userInfo, 0);
         extractedInfo = new Map();
    })
    it(`GET ${endpoints.getUserWithoutAuth} : User info without authorization`, async() => {
        preparedRequest = userRequestService.prepare(endpoints.getUserWithoutAuth, methods.GET, user);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(200);
        expect(response.body[0].username).to.equal(user.username);
    }).timeout(timeout);
    it(`GET ${endpoints.getDetailedUserWithAuth} : User Info post sign in`, async function () {
        preparedRequest = userRequestService.prepare(endpoints.getDetailedUserWithAuth, methods.GET, user);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(200);
        expect(response.body.username).to.equal(user.username);
    }).timeout(timeout);
    it(`POST ${projectEndpoints.createNewPrivateProject} : Create a private project`, async function () {
        preparedRequest = projectsService.prepare(projectEndpoints.createNewPrivateProject, methods.POST, user);
        response = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(201);
        expect(response.body.visibility).to.be.equal('private');
        extractedInfo.set(params.NAME, response.body.name);
    }).timeout(timeout);
    it(`GET ${endpoints.getProjectsList} : list of projects for the signed in user`, async function () {
        preparedRequest = userRequestService.prepare(endpoints.getProjectsList, methods.GET, user);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(200);
        expect(getByIndex(response.body, 0).name).to.be.equal(extractedInfo.get(params.NAME));
        extractedInfo.set(params.ID, getByIndex(response.body, 0).id);
    }).timeout(timeout);
    it(`POST ${projectEndpoints.addMemberToAProject} : Add a new member to the project`, async function () {
        preparedRequest = projectsService.prepare(projectEndpoints.addMemberToAProject, methods.POST,
            user, extractedInfo);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(201);
        extractedInfo.set(params.USER_ID, response.body.id);
    }).timeout(timeout);
    it(`GET ${projectEndpoints.getMembersOfAProject} : get members of a project`, async function () {
        preparedRequest = projectsService.prepare(projectEndpoints.getMembersOfAProject, methods.GET,
            user, extractedInfo);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(200);
        expect(response.body[response.body.length - 1].id).to.be.equal(extractedInfo.get(params.USER_ID));
    }).timeout(timeout);
    it(`POST ${projectEndpoints.addVariableToAProject} and DELETE ${projectEndpoints.deleteVariableFromAProject}
    : add and delete a variable to project`, async function () {
        preparedRequest = projectsService.prepare(projectEndpoints.addVariableToAProject, methods.POST,
            user, extractedInfo);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(201);
        extractedInfo.set(params.KEY, response.body.key);

        preparedRequest = projectsService.prepare(projectEndpoints.deleteVariableFromAProject,
            methods.DELETE, user, extractedInfo);
        response = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(204);
    }).timeout(timeout);
    it(`DELETE ${projectEndpoints.deleteProject} : should delete the project created`, async function () {
        preparedRequest = projectsService.prepare(projectEndpoints.deleteProject, methods.DELETE,
            user, extractedInfo);
        response  = await responseService.getResponse(preparedRequest);

        expect(response.statusCode).to.be.equal(202);
    }).timeout(timeout);
})
