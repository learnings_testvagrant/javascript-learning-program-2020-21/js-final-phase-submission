class GenericRequest {
    constructor(method, path, headers) {
        this.method = method;
        this.path = path;
        this.headers = headers;
    }
    setQueryParams(queryParams) {
        this.queryParams = queryParams;
        return this;
    }
    setBody(payload) {
        this.payload = payload;
        return this;
    }
}

module.exports = GenericRequest;