const genericRequest = require('../models/genericRequest');

class RequestBuilder {

    constructor(method, path, headers) {
        this.method = method;
        this.path = path;
        this.headers = headers;
        this.request = new genericRequest(this.method, this.path, this.headers);
    }

    build() {
        return this.request;
    }
}

module.exports = RequestBuilder;