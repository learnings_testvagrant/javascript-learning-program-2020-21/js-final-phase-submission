# js-final-phase-submission

Description : An API Testing framework with the help of following tools.
   
    Node JS - Framework
    Supertest - Client for http calls
    Chai - Assertions
    Mocha - Test Management
    Mochawesome - Reports

This framework aims to build a scalable, easily maintainable API testing automation solution with mochawesome Reporting. 

It's currently implemented for following scenarios of an existing user in gitlab.

- Get existing user info 
- Add a new project for an existing user
- Add members to a project
- Get list of members of a project
- Get list of projects for a user
- Add/Delete variable to a project
- Delete a project

All the above scenarios are present in tests/basicGitUserFlowsTest.js.

**Steps to run in local** : 

1. Clone the repo
2. Configure any test related data in testData/userData.js or testData/projectData.js
3. Set `PRIVATE_TOKEN = {gitlab-access-token}` as env variable for the user set in step 2
4. Run `npm test`
3. Reports are present in mochawesome-report



**How to set env variable in local** :

> On Mac
- Open Terminal
- Run touch ~/.bash_profile; open ~/.bash_profile
- In TextEdit, add
- export PRIVATE_TOKEN="access-token-value"
- Save the .bash_profile file and Quit (Command + Q) Text Edit.
- Run source ~/.bash_profile

> On Windows
- Search and select System (Control Panel).
- Click on the Advanced system settings link and then click Environment Variables. Under the section System Variables, select the environment variable you want to edit, and click Edit.
- If the environment variable you want doesn't exist, click New.
- Input the value of the environment variable in the Edit System Variable (or New System Variable) window, and click OK.
