function getRandomDataFromAnArray(input) {
    let randomIndex = Math.floor(Math.random() * input.length);
    return input[randomIndex];
}

exports.getUserByIndex = function getParticularUserByIndex(input, index) {
    if(index < input.length && index !== -1) {
        return input[index];
    }
}

exports.replacePathWithDynamicValue = function replacePathWithDynamicValue(path, keyValue) {
    for(let [key, value] of keyValue) {
        path = path.toString().replace(`:${key}`, value);
    }
    return path;
}

exports.getRandomUser = function getRandomUser(userInfo) {
    return getRandomDataFromAnArray(userInfo);
}

exports.getByIndex = function getByIndex(input, index) {
    if(index < input.length && index !== -1) {
        return input[index];
    }
}

exports.generateRandom = function generateRandom(base) {
    base = `${base}${Math.random().toString(36).substring(7)}`;
    return base;
}
