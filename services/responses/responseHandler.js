const {client} = require("../../testData/config");

class ResponseHandler {

    constructor() {
    }

    async sendPostRequest(requestData) {
       return  await
           client
               .post(requestData.path)
               .set(Object.fromEntries(requestData.headers))
               .send(requestData.payload)
               .query(requestData.queryParams)
               .then((response) => response)
               .catch((error) => error.response);
    }

    async sendPutRequest(requestData) {
        return  await
            client
                .put(requestData.path)
                .set(Object.fromEntries(requestData.headers))
                .send(requestData.payload)
                .query(requestData.queryParams)
                .then((response) => response)
                .catch((error) => error.response);
    }

    async sendGetRequest(requestData) {
        return await
            client
                .get(requestData.path)
                .set(Object.fromEntries(requestData.headers))
                .query(requestData.queryParams)
                .then((response) => response)
                .catch((error) => error.response);
    }

    async sendDeleteRequest(requestData) {
        return await
            client
                .delete(requestData.path)
                .set(Object.fromEntries(requestData.headers))
                .query(requestData.queryParams)
                .then((response) => response)
                .catch((error) => error.response);
    }

}

module.exports = ResponseHandler;