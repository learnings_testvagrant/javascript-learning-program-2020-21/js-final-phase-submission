const ResponseHandler = require("./responseHandler");
const {methods, messages} = require("../../constants/constants");

exports.getResponse = async function (request) {
    let responses = new ResponseHandler();
    switch (request.method.toString().toUpperCase()) {
        case(methods.GET) : {
            return await responses.sendGetRequest(request);
        }
        case(methods.POST) : {
            return await responses.sendPostRequest(request);
        }
        case(methods.DELETE) : {
            return await responses.sendDeleteRequest(request);
        }
        case(methods.PUT) : {
            return await responses.sendPutRequest(request);
        }
        default : {
            throw new Error(`${request.method} ${messages.UNSUPPORTED_OR_INVALID_METHOD}`);
        }
    }
}
