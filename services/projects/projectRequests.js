const {createRequest} = require("../common");
const {methods, params, messages} = require("../../constants/constants");
const {projectEndpoints} = require("../../testData/projectData");
const {getByIndex, replacePathWithDynamicValue} = require("../../utils/helper");
const {newProjectData, memberData, variables} = require("../../testData/projectData");
const {commonHeaders, defaultHeaders} = require('../../testData/config');

module.exports.prepare = function prepare(endpoint, method, user, projectInfo = 0) {
    const headers = new Map([...defaultHeaders, ...commonHeaders]);
    const query = {};
    return prepareRequest(user, projectInfo, endpoint, method, query, headers);
}

function prepareRequest(user, projectInfo, endpoint, method, query, headers) {
    switch (`${endpoint}${method}`) {
        case `${projectEndpoints.createNewPrivateProject}${methods.POST}` : {
            return createProject(endpoint, headers, query, user);
        }
        case `${projectEndpoints.getProjectDetailsById}${methods.GET}` : {
            return getProjectDetails(endpoint, headers, query, user);
        }
        case `${projectEndpoints.addMemberToAProject}${methods.POST}` : {
            return addMember(endpoint, headers, query, projectInfo);
        }
        case `${projectEndpoints.addVariableToAProject}${methods.POST}` : {
            return addVariable(endpoint, headers, query, projectInfo);
        }
        case `${projectEndpoints.getMembersOfAProject}${methods.GET}` : {
           return getMembers(endpoint, headers, query, projectInfo, user);
        }
        case `${projectEndpoints.deleteVariableFromAProject}${methods.DELETE}` : {
           return delVariable(endpoint, headers, projectInfo);
        }
        case `${projectEndpoints.getVariablesOfAProject}${methods.GET}` : {
            return getVariables(endpoint, headers, projectInfo);
        }
        case `${projectEndpoints.deleteProject}${methods.DELETE}` : {
            return deleteProject(endpoint, headers, query, projectInfo, user);
        }
        default : {
            throw new Error(messages.INVALID_ENDPOINT);
        }
    }
}

function createProject(endpoint, headers, query, user) {
    const projectToCreate = getByIndex(newProjectData, 0);
    query[params.NAME] = projectToCreate.name;
    query[params.INITIALIZE_WITH_README] = projectToCreate.initialize_with_readme;
    query[params.DESCRIPTION] = projectToCreate.description;
    query[params.MEMBERSHIP] = user.membership;
    return createRequest(methods.POST, endpoint, headers, query);
}

function addMember(endpoint, headers, query, projectInfo) {
    const memberToAdd = getByIndex(memberData, 0);
    query[params.USER_ID] = memberToAdd.user_id;
    query[params.ACCESS_LEVEL] = memberToAdd.access_level;
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.POST, endpoint, headers, query);
}

function getMembers(endpoint, headers, query, projectInfo, user) {
    query[params.MEMBERSHIP] = user.membership;
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.GET, endpoint, headers, query);
}

function addVariable(endpoint, headers, query, projectInfo){
    const varToAdd = getByIndex(variables, 0);
    query[params.ID] = projectInfo.id;
    query[params.KEY] = varToAdd.key;
    query[params.VALUE] = varToAdd.value;
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.POST, endpoint, headers, query);
}

function getVariables(endpoint, headers, projectInfo) {
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.GET, endpoint, headers);
}

function delVariable(endpoint, headers, projectInfo){
    let toReplace = new Map();
    toReplace.set(params.ID, projectInfo.get(params.ID));
    toReplace.set(params.KEY, projectInfo.get(params.KEY));
    endpoint = replacePathWithDynamicValue(endpoint, toReplace);
    return createRequest(methods.DELETE, endpoint, headers);
}

function getProjectDetails(endpoint, headers, query, projectInfo, user) {
    query[params.MEMBERSHIP] = user.membership;
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.GET, endpoint, headers, query);
}

function deleteProject(endpoint, headers, query, projectInfo, user){
    query[params.MEMBERSHIP] = user.membership;
    endpoint = replaceProjectIdInPath(projectInfo, endpoint);
    return createRequest(methods.DELETE, endpoint, headers, query);
}

function replaceProjectIdInPath(projectInfo, endpoint) {
    let toReplace = new Map();
    toReplace.set(params.ID, projectInfo.get(params.ID));
    return replacePathWithDynamicValue(endpoint, toReplace);
}