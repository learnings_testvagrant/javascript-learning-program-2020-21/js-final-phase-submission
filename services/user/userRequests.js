const {createRequest} = require("../common");
const {methods, params, messages} = require("../../constants/constants");
const {endpoints} = require("../../testData/userData");
const {commonHeaders, defaultHeaders} = require('../../testData/config');

module.exports.prepare = function prepare(endpoint, method, user) {
    const headers = new Map([...defaultHeaders, ...commonHeaders]);
    const query = {};
    return prepareRequest(user, endpoint, method, query, headers);
}

function prepareRequest(user, endpoint, method, query, headers) {
    switch (`${endpoint}${method}`) {
        case `${endpoints.getUserWithoutAuth}${methods.GET}` : {
            query[params.USERNAME] = user.username;
            return createRequest(methods.GET, endpoint, defaultHeaders, query);
        }
        case `${endpoints.getDetailedUserWithAuth}${methods.GET}` : {
            return createRequest(methods.GET, endpoint, headers);
        }
        case `${endpoints.getProjectsList}${methods.GET}` : {
            query[params.MEMBERSHIP] = user.membership;
            return createRequest(methods.GET, endpoint, headers, query);
        }
        default : {
            throw new Error(messages.INVALID_ENDPOINT);
        }
    }
}