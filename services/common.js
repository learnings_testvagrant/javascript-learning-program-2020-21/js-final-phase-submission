const RequestBuilder = require("../request/requestBuilder");

exports.createRequest = function createRequest(method , endpoint , headers ,
                                               query = 0, payload = 0) {
    const preparedRequest = new RequestBuilder(method, endpoint, headers).build();
    if (query !== 0)
        preparedRequest.setQueryParams(query);
    if (payload !== 0)
        preparedRequest.setBody(payload);
    return preparedRequest;
}